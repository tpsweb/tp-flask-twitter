#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from flask import request
from flask import Response
from flask import render_template
from threading import Lock
import time
import json

app = Flask(__name__)


class User:
    """
    Un utilisateur a un nom et un age.
    L'id est celui généré par le système lors de l'insertion
    """
    def __init__(self, name, age):
        self.id = -1
        self.name = name
        self.age = age

    def __str__(self):
        """
        Redéfinition de la fonction "toString" d'un objet User.
        example :
        user = User("tom", 22)
        print(user) -> affiche la string "name : tom, age : 22"
        """
        return "name : {}, age : {}".format(self.name, str(self.age))

    def get_values(self):
        """ 
        Transforme une instance de la classe User en dictionnaire python
        """
        return {
          "id": self.id,
          "name": self.name,
          "age": self.age
        }

    def to_json(self):
        """
        Transforme une instance de la classe User en json.
        example :
        user = User("tom", 22)
        user.to_json -> { "name" : "tom", "age" : 22 }
        """
        return json.dumps(self.get_values(), indent=2)

"""
Conserve la liste des utilisateurs enregistrés
"""
users = {}
"""
Conserve l'id du prochain user à créer.
next_id correspond donc aux nombres d'utilisateurs déjà créé dans le système.
"""
next_id = 0

lock = Lock()

"""
Définition des routes et des fonction associées
"""


@app.route('/')
def index():
    """
    Le fichier index.html se situe dans le répertoire template
    """
    return render_template('index.html')


@app.route('/list-users/')
def page_list_users():
    """
    Page HTML pour lister les utilisateurs
    """
    return render_template('list-users.html')


@app.route('/create-user/')
def page_create_user():
    """
    Page HTML avec un formulaire pour créer un utilisateur
    """
    return render_template('create-user.html')


@app.route('/users/', methods=['GET'])
def get_users():
    """
    Renvoie la liste de tous les utilisateurs enregistrés
    """
    resp_users = [users[u].get_values() for u in users]
    print(users)
    return Response(json.dumps(resp_users, indent=2),
                    status=200,
                    mimetype='application/json')


@app.route('/users/', methods=['POST'])
def create_user():
    """
    Créer un utilisateur et renvoie son id
    """
    global next_id
    data = json.loads(request.data)
    new_user = User(data['name'], data['age'])
    new_user.id = next_id
    users[next_id] = new_user
    # on simule un temps d'insertion dans une base de données
    # time.sleep(0.1) attend 0.1 s
    time.sleep(0.1)
    next_id += 1
    return Response(response=json.dumps({"id": new_user.id}),
                    status=200,
                    mimetype='application/json')


@app.route('/users/<user_id>')
def get_user(user_id):
    """
    Renvoie les informations de l'utilisateur d'id user_id
    Retourne une erreur 404 en cas d'erreur
    """
    try:
        user = users[int(user_id)]
        return Response(response=user.to_json(),
                        status=200,
                        mimetype='application/json')
    except:
        return Response(status=404)


@app.route('/users/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    """
    Supprime un utilisateur
    """
    # @todo delete the user with the given id, 404 if user does not exist
    return Response(status=405)


if __name__ == '__main__':
    app.run(debug=True, threaded=False, port=5000)

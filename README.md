# Mon mini Twitter en Flask

L'objectif du TP est d'écrire une application réalisant un twitter minimal.
Cette application est basée sur le framework [FLASK](http://flask.pocoo.org/) pour construire une API REST. L'implémentation donnée est incomplète et c'est à vous de la compléter.

## Installation de l'environnement Flask

### Récupération des fichiers du TP

Le TP est disponible sur : ```/share/m1miage/CSR/tp4-flask-twitter.zip```
Copier le et décompresser le sur votre espace personnel.

### Installation de virtualenv

Virtualenv permet d'utiliser des environnements python dans votre espace utilisateur. Il permet par exemple d'installer toutes les librairies nécessaires
à un projet sans les permissions administrateur. Les commandes suivantes sont à taper dans une console (# signale un commentaire et donc n'est pas à taper).

```
# téléchargement de l'outil virtualenv
$) curl -O https://pypi.python.org/packages/source/v/virtualenv/virtualenv-1.9.tar.gz
$) tar xvfz virtualenv-1.9.tar.gz

# installation d'un virtual env pour le TP
$) python virtualenv-1.9/virtualenv.py tp-flask-env
$) cd tp-flask-env
$) ls
# vous devriez voir 3 sous répertoires :
# bin     include lib

# on active le virtualenv
$) source bin/activate
```

Note :
 Désormais tous les bibliothèques python installé dans le répertoire ```tp-flask-env```

### Installation de Flask

```pip install flask```

## Premiers tests

* Lancer le serveur

```
# Dans le répertoire du tp
$) python twitter.py
* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

Le serveur est lancé et écoute sur le port 5000 de votre machine localhost.

> Connecter votre navigateur web à l'adresse : ```http://127.0.0.1:5000```,
un client web minimal vous est présenté. Certaines fonctionnalités restent à implémenter.

> Les pages affichées par le navigateurs fonctionnent en 2 temps, par exemple
 le chargement de la page ```/list_users/``` suit le schéma :

 ```

 navigateur                                serveur
 GET /list_users ------------------------>
                                           prépare et rend la page statique
                                           templates/list_users.html
page présentant <------------------------
un tableau vide            (html)

remplissage du tableau (script javascript dans list_users.html)
GET /users      ------------------------->  
                                           prépare et rend la liste des utilisateurs
remplit le tableau <-----------------------
                            (json)                                   
 ```

* Parallèlement, ouvrir un éditeur de texte pour pouvoir éditer le fichier ```twitter.py```.

* Ouvrir une autre console, regardez le contenu de ```create_users.sh```.

> Que fait ce script ? Lancer le et observer son effet :

```
./create_users.sh
```

## Accepter des connections concurrentes

On accepte les connections concurrentes (un thread par requête) en modifiant la ligne  qui démarre le serveur comme suit :
```
app.run( ...,threaded = True , ...)
```
> Relancer le script de création d'utilisateurs

> Qu'observez-vous ?

> Corriger l'implementation à l'aide de  [ceci](https://docs.python.org/2/library/threading.html#lock-objects)

## Coder les nouvelles fonctionnalités

On désire que le serveur puisse suivre cette spécification :


|URI                     | command   | description                     |
|------------------------|-----------|---------------------------------|
|/users                  | GET       | returns the list of users       |
|/users                  | POST      | add a user                      |
|/users/{userId}         | GET       | retrieve a user (and its tweets)|
|/users/{userId}         | DELETE    | deletes the user                |
|/users/{userId}/tweets  | POST      | add a tweet to this user        |
|/users/{userId}/tweets  | GET       | returns the tweets of this user |



> Ajouter une classe ```Tweet``` et ajouter à la classe ```User``` la possibilité de conserver une liste de ```Tweet``` en mémoire

> Ajouter les fonctions responsables de traiter les requêtes arrivant sur l'URI : ```/users/{userId}/tweets```

> Coder les fonctionnalités manquantes
